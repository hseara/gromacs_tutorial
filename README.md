# README #

Latex source files of an introductory tutorial for gromacs. It will continuously be updated to match the latest available gromacs version.
 
It is useful for previous gromacs versions, but minor changes in the commands are to be expected.

# COMPILATION INSTRUCTIONS #
Please use the following command to compile the document:
`pdflatex -synctex=1 -interaction=nonstopmode -enable-write18 %.tex`

# LINKS to tutorial PDF and Files #
Link to the compiled [pdf](https://zenodo.org/record/3569722/files/Introduction_to_Gromacs2019.pdf)

Link to the excercise [data](https://zenodo.org/record/3569722/files/task_material.tar.gz)

# Citation #
Please use the following ["DOI: 10.5281/zenodo.1010562"](https://zenodo.org/record/1010462).

